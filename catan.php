<canvas id="catan" width="765" height="670"></canvas>
<script>
// Hexagon
function Hexagon(ctx, color, pos, size, scale) {
    this.color = color;
    this.ctx = ctx;
    this.x = pos[0];
    this.y = pos[1];
    this.z = pos[2] || 0; // scale
    this.width = size;
    this.height = size;
}
// Hexagon.draw
Hexagon.prototype.draw = function (x, y) {
    if (x == null || y == null) {
        x = this.x;
        y = this.y; 
    }
    var width  = this.width  + (this.width  * this.z), // scaled width
        height = this.height + (this.height * this.z), // scaled height
        cx  = x * (width + dist) - y * (width + dist) / 2,
        cy  = y * (3/4 * height + dist) - 20,
        ctx = this.ctx;
    ctx.fillStyle = this.color;
    ctx.beginPath();
    ctx.moveTo(cx, cy - height/2);
    ctx.lineTo(cx + width/2, cy - height/4);
    ctx.lineTo(cx + width/2, cy + height/4);
    ctx.lineTo(cx, cy + height/2);
    ctx.lineTo(cx - width/2, cy + height/4);
    ctx.lineTo(cx - width/2, cy - height/4);
    ctx.lineTo(cx, cy - height/2);  
    ctx.fill();
	if(this.color=='#FFF1AB'){
		var img = new Image();
		img.src = './images/thief.png';
		img.onload = function(){
		ctx.drawImage(img,cx-15,cy-48);
	}
	}
};
var canvas = document.getElementById('catan');
if (canvas.getContext) { 
    var ctx = canvas.getContext('2d');
    var dist = 2;

    // Create Hexagons
    var size = 150;
    var brick = '#CC6359';
    var ore = '#372D2D';
    var desert = '#FFF1AB';
    var sheep = '#8AFF70';
    var wood = '#409240';
    var wheat = '#F7EC66';

    var colors = [
        brick, brick, brick,
        ore, ore, ore, 
        desert, 
        sheep, sheep, sheep, sheep, 
        wood, wood, wood, wood, 
        wheat, wheat, wheat, wheat 
    ];

    /**
    * it shuffles an array
    *
    * @param Array a
    * @return Array
    */
    function shuffle (a) {
        var j, x, i;
        for (i = a.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
        return a;
    }

    shuffle(colors);

    var hexes = [
        new Hexagon(ctx, colors.pop(), [2, 1], size),
        new Hexagon(ctx, colors.pop(), [3, 1], size),
        new Hexagon(ctx, colors.pop(), [4, 1], size),
        new Hexagon(ctx, colors.pop(), [2, 2], size),
        new Hexagon(ctx, colors.pop(), [3, 2], size),
        new Hexagon(ctx, colors.pop(), [4, 2], size),
        new Hexagon(ctx, colors.pop(), [5, 2], size),
    	new Hexagon(ctx, colors.pop(), [2, 3], size),
        new Hexagon(ctx, colors.pop(), [3, 3], size),
        new Hexagon(ctx, colors.pop(), [4, 3], size),
        new Hexagon(ctx, colors.pop(), [5, 3], size),
        new Hexagon(ctx, colors.pop(), [6, 3], size),
        new Hexagon(ctx, colors.pop(), [3, 4], size),
        new Hexagon(ctx, colors.pop(), [4, 4], size),
    	new Hexagon(ctx, colors.pop(), [5, 4], size),
    	new Hexagon(ctx, colors.pop(), [6, 4], size),
    	new Hexagon(ctx, colors.pop(), [4, 5], size),
    	new Hexagon(ctx, colors.pop(), [5, 5], size),
    	new Hexagon(ctx, colors.pop(), [6, 5], size),
    ];

    function draw() {
        for (var i = hexes.length; i--;) {
            hexes[i].draw();
        }
    }

    draw();
}
</script>