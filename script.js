var neverending = 0,
	save = 400,
	load = 1100;

function replace() {
    $('#replace').load('questions.php', function() {
        neverending++;
        $('#replace').prepend((neverending - 1) + '. ');
        $('#continue').show();
        if (neverending == 11) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '10 questions']);
        if (neverending == 21) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '20 questions']);
        if (neverending == 31) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '30 questions']);
        if (neverending == 41) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '40 questions']);
        if (neverending == 51) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '50 questions']);
        if (neverending == 61) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '60 questions']);
        if (neverending == 71) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '70 questions']);
        if (neverending == 81) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '80 questions']);
        if (neverending == 91) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '90 questions']);
        if (neverending == 101) _gaq.push(['_trackEvent', 'Did Neverending Quiz', '100 questions!']);
    });
}
$('#won').one('click', function() {
    _gaq.push(['_trackEvent', 'Claimed Victory']);
    $('#replace').html('Congratulations! Finally someone took him down! Please take some time, fill out all of the details to make josh\'s shame all the more real.');
    $('#continue').show();
    neverending = 1;
});
$('#temp').on('keypress', function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) $('#continue').click();
});
$('#continue').click(function() {
    if (neverending < 1) return;
    if ($('#temp').length > 0) {
        if ($.trim($('#temp').val()) == '') {
            alert('Please fill in the details.');
            return;
        }
    }
    save += 370;
    load += 495;
    $('#continue').hide();
    $('#replace').html('Saving... <img src="/images/ajax-loader.gif" width="32" height="32" style="position:absolute;margin-top:-6px;margin-left:6px;" />');
    var t = setTimeout(function() {
        $('#replace').html('Loading... <img src="/images/ajax-loader.gif" width="32" height="32" style="position:absolute;margin-top:-6px;margin-left:6px;" />');
        var n = setTimeout(function() {
            replace();
        }, load);
    }, save);
});
$('#lost').submit(function(e) {
    if ($('#name').val() == null || $.trim($('#name').val()) == '') {
        e.preventDefault();
        alert('Please enter your name, heathen!');
    }
});
$('#youTube').delay(700).animate({
    width: '560px',
    height: '315px',
    top: '165px'
}, 1400, "easeInExpo", function() {
    $(this).addClass('glow').find('object').css('visibility', 'visible');
});

function printImage() {
    var canvasData = canvas.toDataURL("image/jpg");
    var ajax = new XMLHttpRequest();
    ajax.open("POST", 'testSave.php', false);
    ajax.setRequestHeader('Content-Type', 'application/upload');
    ajax.send(canvasData);
}
$('#saveImage').one('click', function() {
    printImage();
    $(this).after('<a href="/test.jpg" target="_blank">Save Image</a>');
    _gaq.push(['_trackEvent', 'Created Catan Image']);
});
